from keras.models import Model
from keras.layers import Conv2D, MaxPooling2D, Dropout, Concatenate, UpSampling2D
from utils.utils import *


def build_unet(input, ker_init, dropout, kernel_regularizer=None):
    conv1 = Conv2D(32, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(input)
    conv1 = Conv2D(32, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv1)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = Conv2D(64, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool1)
    conv2 = Conv2D(64, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv2)

    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool2)
    conv3 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv3)

    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool3)
    conv4 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv4)

    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)
    conv5 = Conv2D(512, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool4)
    conv5 = Conv2D(512, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv5)
    drop5 = Dropout(dropout)(conv5)

    up6 = UpSampling2D(size=(2, 2))(drop5)
    up6 = Conv2D(256, 2, activation='relu', padding='same',
                 kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(up6)
    merge6 = Concatenate(axis=3)([conv4, up6])
    conv6 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge6)
    conv6 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv6)

    up7 = UpSampling2D(size=(2, 2))(conv6)
    up7 = Conv2D(128, 2, activation='relu', padding='same',
                 kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(up7)
    merge7 = Concatenate(axis=3)([conv3, up7])
    conv7 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge7)
    conv7 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv7)

    up8 = UpSampling2D(size=(2, 2))(conv7)
    up8 = Conv2D(64, 2, activation='relu', padding='same',
                 kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(up8)
    merge8 = Concatenate(axis=3)([conv2, up8])
    conv8 = Conv2D(64, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge8)
    conv8 = Conv2D(64, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv8)

    up9 = UpSampling2D(size=(2, 2))(conv8)
    up9 = Conv2D(32, 2, activation='relu', padding='same',
                 kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(up9)
    merge9 = Concatenate(axis=3)([conv1, up9])
    conv9 = Conv2D(32, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge9)
    conv9 = Conv2D(32, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv9)

    conv10 = Conv2D(NUM_CLASSES, 1, activation='softmax')(conv9)

    return Model(inputs=input, outputs=conv10, name="U-Net")
