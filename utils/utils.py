import tensorflow as tf


SEGMENT_CLASSES = {
    0: 'NOT tumor',
    1: 'NECROTIC/CORE',
    2: 'EDEMA',
    3: 'ENHANCING'
}


NUM_CLASSES = 4

VOLUME_SLICES = 100
VOLUME_START_AT = 22


TRAIN_DATASET_PATH = '/storage/sorinaau/data/dataset/training_data/'
VALIDATION_DATASET_PATH = '/storage/sorinaau/data/dataset/validation_data'


RESNET50_WEIGHTS_PATH = "/storage/sorinaau/disertation_thesis/weights/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5"


IMG_SIZE = 128
MRI_MODALITIES = 4
INPUT_SHAPE = (IMG_SIZE, IMG_SIZE, MRI_MODALITIES)


def lr_scheduler(epoch, lr):
    # Cosine annealing
    return lr * tf.math.cos(tf.constant(epoch) * (3.14 / 20))
